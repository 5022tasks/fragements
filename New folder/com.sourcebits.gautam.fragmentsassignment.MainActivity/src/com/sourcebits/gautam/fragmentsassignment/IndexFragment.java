package com.sourcebits.gautam.fragmentsassignment;

import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

/**
 * IndexFragament displays the device Manufactures list
 * 
 * @author Gautam B
 * 
 */
public class IndexFragment extends ListFragment {
	private static final String TAG = IndexFragment.class.getCanonicalName();

	/**
	 * Constructor of the Index fragment
	 */
	public IndexFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "onCreate()");
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.d(TAG, "onCreateView()");
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	/**
	 * set the manufactures list from the string.xml
	 */
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		CustomAdapter customAdapter = new CustomAdapter(getActivity(),
				getResources().getStringArray(R.array.manufactures));
		setListAdapter(customAdapter);
	}

	/**
	 * On click of the list item it populates a new list
	 */
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		Log.d("manu", "pos: " + position);

		ProductFragment details = new ProductFragment();
		details.setPos(position);
		FragmentTransaction fTrans = getFragmentManager().beginTransaction();
		fTrans.replace(R.id.fragment_container2, details);
		fTrans.commit();
	}

}