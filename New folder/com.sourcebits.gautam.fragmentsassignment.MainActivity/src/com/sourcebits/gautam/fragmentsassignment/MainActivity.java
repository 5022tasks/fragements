package com.sourcebits.gautam.fragmentsassignment;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.Menu;

/**
 * Main Activity start screens which loads the manufactue's List
 * 
 * @author Gautam
 * 
 */
public class MainActivity extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		/**
		 * dynamically creating a new fragment and laoding it to the container
		 */
		android.app.FragmentManager fragmentManager = getFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		IndexFragment deviceManufactures = new IndexFragment();
		fragmentTransaction.add(R.id.fragment_container0, deviceManufactures,
				"deviceManufactures");
		fragmentTransaction.commit();
	}

}
